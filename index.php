<?php 
    require(Animal.php);
    require(Ape.php);
    require(Frog.php);

    $object = new Animal("shaun");

    echo "Nama Hewan : .$object->name <br>";
    echo "Jumlah Kaki : .$object->legs <br>";
    echo "Berdarah dingin : . $object->cold_blooded <br>";

    $sungokong = new Ape("kera sakti");

    echo "Nama Hewan : .$sungokong->name <br>";
    echo "Jumlah Kaki : .$sungokong->legs <br>";
    echo "Bedarah dingin : .$sungokong->cold_blooded <br>";
    echo "yelyel : .$sungokong->yell() <br>";

    $kodok = new Frog("buduk");

    echo "Nama Hewan : .$kodok->name <br>";
    echo "Jumlah Kaki : .$kodok->legs <br>";
    echo "Bedarah dingin : .$kodok->cold_blooded <br>";
    echo "yelyel : . $kodok->jump() <br>";
?>